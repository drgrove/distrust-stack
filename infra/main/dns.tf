resource "digitalocean_domain" "default" {
  name = "distrust.co"
}

resource "digitalocean_record" "main" {
  domain = digitalocean_domain.default.id
  type   = "A"
  name   = "@"
  value  = "143.198.235.76"
}

resource "digitalocean_record" "billing" {
  domain = digitalocean_domain.default.id
  type   = "A"
  name   = "billing"
  value  = "45.16.98.153"
}

resource "digitalocean_record" "www" {
  domain = digitalocean_domain.default.id
  type   = "CNAME"
  name   = "www"
  value  = "${digitalocean_domain.default.id}."
}

resource "digitalocean_record" "mx1-main" {
  domain   = digitalocean_domain.default.id
  type     = "MX"
  name     = "@"
  priority = 10
  value    = "aspmx1.migadu.com."
}

resource "digitalocean_record" "mx2-main" {
  domain   = digitalocean_domain.default.id
  type     = "MX"
  name     = "@"
  priority = 20
  value    = "aspmx2.migadu.com."
}

## MX subdomain wildcard
resource "digitalocean_record" "mx1-wildcard" {
  domain   = digitalocean_domain.default.id
  type     = "MX"
  name     = "*"
  priority = 10
  value    = "aspmx1.migadu.com."
}

resource "digitalocean_record" "mx2-wildcard" {
  domain   = digitalocean_domain.default.id
  type     = "MX"
  name     = "*"
  priority = 20
  value    = "aspmx2.migadu.com."
}

resource "digitalocean_record" "mail-verification" {
  domain = digitalocean_domain.default.id
  type   = "TXT"
  name   = "@"
  value  = "hosted-email-verify=kezkgvsn"
}

## DKIM+ARC
resource "digitalocean_record" "mail-dkim-primary" {
  domain = digitalocean_domain.default.id
  type   = "CNAME"
  name   = "key1._domainkey"
  value  = "key1.distrust.co._domainkey.migadu.com."
}

resource "digitalocean_record" "mail-dkim-secondary" {
  domain = digitalocean_domain.default.id
  type   = "CNAME"
  name   = "key2._domainkey"
  value  = "key2.distrust.co._domainkey.migadu.com."
}

resource "digitalocean_record" "mail-dkim-tertiary" {
  domain = digitalocean_domain.default.id
  type   = "CNAME"
  name   = "key3._domainkey"
  value  = "key3.distrust.co._domainkey.migadu.com."
}

resource "digitalocean_record" "mail-spf" {
  domain = digitalocean_domain.default.id
  type   = "TXT"
  name   = "@"
  value  = "v=spf1 include:spf.migadu.com -all"
}

resource "digitalocean_record" "mail-dmarc" {
  domain = digitalocean_domain.default.id
  type   = "TXT"
  name   = "_dmarc"
  value  = "v=DMARC1; p=quarantine;"
}

resource "digitalocean_record" "mail-discovery" {
  domain = digitalocean_domain.default.id
  type   = "CNAME"
  name   = "autoconfig"
  value  = "autoconfig.migadu.com."
}

resource "digitalocean_record" "mail-src-autodiscover" {
  domain   = digitalocean_domain.default.id
  type     = "SRV"
  name     = "_autodiscover._tcp"
  port     = 443
  priority = 0
  weight   = 1
  value    = "smtp.migadu.com"
}

resource "digitalocean_record" "mail-srv-submissions" {
  domain   = digitalocean_domain.default.id
  type     = "SRV"
  name     = "_submissions._tcp"
  port     = 465
  priority = 0
  weight   = 1
  value    = "smtp.migadu.com"
}

resource "digitalocean_record" "mail-srv-imaps" {
  domain   = digitalocean_domain.default.id
  type     = "SRV"
  name     = "_imaps._tcp"
  port     = 993
  priority = 0
  weight   = 1
  value    = "imap.migadu.com"
}

resource "digitalocean_record" "mail-srv-pop3s" {
  domain   = digitalocean_domain.default.id
  type     = "SRV"
  name     = "_pop3s._tcp"
  port     = 995
  priority = 0
  weight   = 1
  value    = "pop.migadu.com"
}
