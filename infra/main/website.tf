locals {
    content_types = {
      css  = "text/css"
      html = "text/html"
      js   = "application/javascript"
      json = "application/json"
      txt  = "text/plain"
      png  = "image/png"
      svg  = "image/svg+xml"
    }
}

resource "digitalocean_spaces_bucket" "website" {
  name   = "website-${random_id.suffix.hex}"
  region = var.region
  acl    = "public-read"
}

resource "digitalocean_spaces_bucket_object" "website" {
  for_each = fileset("${var.out_dir}/website", "**")
  region   = var.region
  bucket   = digitalocean_spaces_bucket.website.name
  source   = "${var.out_dir}/website/${each.value}"
  key      = each.value
  etag     = filemd5("${var.out_dir}/website/${each.value}")
  content_type = lookup(local.content_types, element(split(".", each.value), length(split(".", each.value)) - 1), "text/plain")
  acl      = "public-read"
  content_encoding = "utf-8"
}

resource "tls_private_key" "website" {
  algorithm = "RSA"
}

resource "digitalocean_certificate" "website" {
  name = "le-terraform-example"
  type = "lets_encrypt"
  domains = [
    digitalocean_domain.default.name,
    format("%s%s", "www.", digitalocean_domain.default.name)
  ]
}

resource "digitalocean_cdn" "website" {
  origin           = digitalocean_spaces_bucket.website.bucket_domain_name
  certificate_name = digitalocean_certificate.website.name
  depends_on = [
    digitalocean_spaces_bucket.website
  ]
}

# ## Handle record for CDN redirect
# resource "digitalocean_record" "cdn" {
#   domain = digitalocean_domain.default.name
#   type   = "CNAME"
#   name   = digitalocean_cdn.distrust_co.origin
#   value  = "${digitalocean_domain.default.name}."
#   depends_on = [
#     digitalocean_cdn.distrust_co
#   ]
# }

# # Output the endpoint for the CDN resource
# output "fqdn" {
#   value = digitalocean_cdn.distrust_co.endpoint
# }
# #
# output "cdn_origin" {
#   value = digitalocean_cdn.distrust_co.origin
# }

# # Handle record for distrust.co
# resource "digitalocean_record" "distrust_co-cdn" {
#   domain = digitalocean_domain.default.name
#   type   = "A"
#   name   = "@"
#   value  = "143.198.235.76"
#   depends_on = [
#     digitalocean_cdn.distrust_co
#   ]
# }

