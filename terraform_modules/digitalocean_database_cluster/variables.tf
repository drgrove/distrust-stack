variable "cluster_name" {
  type = string
}

variable "db_engine" {
  type = string
}

variable "size" {
  type = string
}

variable "digitalocean_region" {
  type = string
}

variable "node_count" {
  type = number
}

variable "databases" {
  type = list(object({
    name = string,
    create_default_superuser = optional(bool, false),
  }))
}

variable "db_version" {
  type = string
}

variable "vpc_id" {
  type = string
  nullable = true
}

variable "dbcli_name" {
  type    = string
  default = "psql"
}
