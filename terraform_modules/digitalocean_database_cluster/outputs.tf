output "database_cluster" {
  value = digitalocean_database_cluster.main
}

output "database_users" {
  value = digitalocean_database_user.default_users
}
