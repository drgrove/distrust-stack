variable "talos_cluster_name" {
  type = string
  description = "Name of the Talos cluster"
}

variable "talos_image" {
  type = string
  description = "Talos image to load to cluster"
}

variable "talos_config_directory" {
  type = string
  default = "talosconfig"
  description = "Directory name in the root Terraform directory to place Talos configuration"
}

variable "digitalocean_region" {
  type = string
  description = "Human-readable slug of the DigitalOcean region"
}

variable "control_plane_pool" {
  type = object({
    count = optional(number, 1),
    size = optional(string, "s-2vcpu-4gb"),
  })
  default = {}
}

variable "worker_pools" {
  type = list(object({
    name = string,
    count = optional(number, 1),
    size = optional(string, "s-2vcpu-4gb"),
  }))
}

variable "vpc_id" {
  type = string
  default = "undefined"
}
