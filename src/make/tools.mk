.PHONY: tools
tools: tools-src \
	$(OUT_DIR)/tofu.linux-x86_64 \
	$(OUT_DIR)/sops.linux-x86_64 \
	$(OUT_DIR)/talosctl.linux-x86_64 \
	$(OUT_DIR)/kubectl.linux-x86_64 \
	$(OUT_DIR)/helm.linux-x86_64 \
	$(OUT_DIR)/kustomize.linux-x86_64 \
	$(OUT_DIR)/ksops-exec.linux-x86_64

.PHONY: tools-src
tools-src: \
	$(FETCH_DIR)/go.src.tar.gz \
	$(FETCH_DIR)/tofu.tar.gz \
	$(FETCH_DIR)/sops.tar.gz \
	$(FETCH_DIR)/talos.tar.gz \
	$(FETCH_DIR)/kubernetes.tar.gz \
	$(FETCH_DIR)/helm.tar.gz \
	$(FETCH_DIR)/kustomize.tar.gz \
	$(FETCH_DIR)/ksops.tar.gz

.PHONY: tools-install
tools-install: tools
	install -D $(OUT_DIR)/tofu.$(HOST_OS)-$(HOST_ARCH) $(PREFIX)/bin/tofu
	install -D $(OUT_DIR)/sops.$(HOST_OS)-$(HOST_ARCH) $(PREFIX)/bin/sops
	install -D $(OUT_DIR)/talosctl.$(HOST_OS)-$(HOST_ARCH) $(PREFIX)/bin/talosctl
	install -D $(OUT_DIR)/kubectl.$(HOST_OS)-$(HOST_ARCH) $(PREFIX)/bin/kubectl
	install -D $(OUT_DIR)/helm.$(HOST_OS)-$(HOST_ARCH) $(PREFIX)/bin/helm
	install -D $(OUT_DIR)/kustomize.$(HOST_OS)-$(HOST_ARCH) $(PREFIX)/bin/kustomize
	install -D $(OUT_DIR)/ksops-exec.$(HOST_OS)-$(HOST_ARCH) $(XDG_CONFIG_HOME)/kustomize/plugin/viaduct.ai/v1/ksops/ksops

$(FETCH_DIR)/go.src.tar.gz:
	$(call fetch_file,$(GO_URL),$(GO_HASH))

$(CACHE_DIR)/bin/go: \
	$(FETCH_DIR)/go.src.tar.gz
	$(call toolchain," \
		mkdir -p $(CACHE_DIR)/src/go $(CACHE_DIR)/lib/go $(CACHE_DIR)/bin \
		&& tar -xf $< -C $(CACHE_DIR)/src \
		&& export GOROOT="" GOROOT_FINAL="$(GOROOT)" \
		&& env -C $(CACHE_DIR)/src/go/src \
			./make.bash \
		&& env -C $(CACHE_DIR)/src/go \
			cp -a pkg src lib misc api test ../../lib/go \
		&& cp $(CACHE_DIR)/src/go/bin/* $(CACHE_DIR)/bin \
	")

$(FETCH_DIR)/talos.tar.gz:
	$(call git_archive,$(TALOSCTL_REPO),$(TALOSCTL_REF))

$(OUT_DIR)/talosctl.linux-x86_64: \
	$(FETCH_DIR)/talos.tar.gz \
	$(CACHE_DIR)/bin/go
	$(call toolchain," \
		mkdir -p $(CACHE_DIR)/src/talosctl \
		&& tar -xzf $< -C $(CACHE_DIR)/src/talosctl \
		&& env -C $(CACHE_DIR)/src/talosctl \
			go build -o /home/build/$@ ./cmd/talosctl \
	")

$(FETCH_DIR)/kubernetes.tar.gz:
	$(call git_archive,$(KUBERNETES_REPO),$(KUBERNETES_REF))

$(OUT_DIR)/kubectl.linux-x86_64: \
	$(FETCH_DIR)/kubernetes.tar.gz \
	$(CACHE_DIR)/bin/go
	$(call toolchain," \
		mkdir -p $(CACHE_DIR)/src/kubernetes \
		&& tar -xvzf $< -C $(CACHE_DIR)/src/kubernetes \
		&& env -C $(CACHE_DIR)/src/kubernetes \
			go build -o /home/build/$@ ./cmd/kubectl \
	")

$(FETCH_DIR)/sops.tar.gz:
	$(call git_archive,$(SOPS_REPO),$(SOPS_REF))

$(OUT_DIR)/sops.linux-x86_64: \
	$(FETCH_DIR)/sops.tar.gz \
	$(CACHE_DIR)/bin/go
	$(call toolchain," \
		mkdir -p $(CACHE_DIR)/src/sops \
		&& tar -xvzf $< -C $(CACHE_DIR)/src/sops \
		&& env -C $(CACHE_DIR)/src/sops \
			go build -o /home/build/$@ ./cmd/sops \
	")

$(FETCH_DIR)/helm.tar.gz:
	$(call git_archive,$(HELM_REPO),$(HELM_REF))

$(OUT_DIR)/helm.linux-x86_64: \
	$(FETCH_DIR)/helm.tar.gz \
	$(CACHE_DIR)/bin/go
	$(call toolchain," \
		mkdir -p $(CACHE_DIR)/src/helm \
		&& tar -xvzf $< -C $(CACHE_DIR)/src/helm \
		&& env -C $(CACHE_DIR)/src/helm \
			go build -o /home/build/$@ ./cmd/helm \
	")

$(FETCH_DIR)/ksops.tar.gz:
	$(call git_archive,$(KSOPS_REPO),$(KSOPS_REF))

$(OUT_DIR)/ksops-exec.linux-x86_64: \
	$(FETCH_DIR)/ksops.tar.gz \
	$(CACHE_DIR)/bin/go
	$(call toolchain," \
		mkdir -p $(CACHE_DIR)/src/ksops \
		&& tar -xvzf $< -C $(CACHE_DIR)/src/ksops \
		&& env -C $(CACHE_DIR)/src/ksops \
			go build -o /home/build/$@ \
	")

$(FETCH_DIR)/tofu.tar.gz:
	$(call git_archive,$(TOFU_REPO),$(TOFU_REF))

$(OUT_DIR)/tofu.linux-x86_64: \
	$(FETCH_DIR)/tofu.tar.gz \
	$(CACHE_DIR)/bin/go
	$(call toolchain," \
		mkdir -p $(CACHE_DIR)/src/tofu \
		&& tar -xvzf $< -C $(CACHE_DIR)/src/tofu \
		&& env -C $(CACHE_DIR)/src/tofu \
			go build -o /home/build/$@ \
	")

$(FETCH_DIR)/kustomize.tar.gz:
	$(call git_archive,$(KUSTOMIZE_REPO),$(KUSTOMIZE_REF))

$(OUT_DIR)/kustomize.linux-x86_64: \
	$(FETCH_DIR)/kustomize.tar.gz \
	$(CACHE_DIR)/bin/go
	$(call toolchain," \
		mkdir -p $(CACHE_DIR)/src/kustomize \
		&& tar -xvzf $< -C $(CACHE_DIR)/src/kustomize \
		&& env -C $(CACHE_DIR)/src/kustomize/kustomize \
			go build -o /home/build/$@ \
	")
