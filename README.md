# Distrust Infrastructure

For the purpose of transparency, we include our infrastructure configuration right out in the open to encourage those we work with, and otherwise to do the same.

## Dependencies

* Docker
* GNU Make

## Usage

```shell
$ make
```
