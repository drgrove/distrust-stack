Kustomizations should be deployed in a specific order to ensure Custom Resource
Definitions or Services used by additional Kustomizations have been deployed
before the Kustomization exists.

Current order:

* Cilium
* DigitalOcean Secrets
* Cert Manager (Deploy twice)
* DigitalOcean (Deploy twice)
* Ingress NGINX
* External DNS
* Keycloak
* Forgejo

Any secrets necessary for any of the previous Kustomizations can be generated
via scripts in the relevant Kustomization. There should be information in the
Kustomization's README (which may be in a `docs` subdirectory) about how to
generate the secrets.
