#!/bin/sh

if test -t 1; then
  # This is not foolproof. Can easily be beat by doing |cat. This is just to
  # make it less likely that secrets are output to terminal.
  echo "Error: Not outputting secret to stdout; redirect output to a file or" \
    "pipe output to \`sops\`." >/dev/stderr
  exit 1
fi

printf_stderr() {
  printf "$@" > /dev/stderr
}

printf_stderr "DigitalOcean VPC ID: "
read DO_VPC_ID
printf_stderr "DigitalOcean Token: "
stty -echo
read DO_TOKEN
stty echo
echo > /dev/stderr

cat <<EOF
apiVersion: v1
kind: Secret
metadata:
  name: digitalocean
stringData:
  vpc-id: ${DO_VPC_ID}
  access-token: ${DO_TOKEN}
EOF
