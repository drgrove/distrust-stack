#!/bin/sh

if test -t 1; then
  # This is not foolproof. Can easily be beat by doing |cat. This is just to
  # make it less likely that secrets are output to terminal.
  echo "Error: Not outputting secret to stdout; redirect output to a file or" \
    "pipe output to \`sops\`." >/dev/stderr
  exit 1
fi

KC_ADMIN=admin
KC_ADMIN_PASSWORD="$(pwgen 32 1)"

cat <<EOF
apiVersion: v1
kind: Secret
metadata:
  name: keycloak-config
stringData:
  admin: ${KC_ADMIN}
  admin_password: ${KC_ADMIN_PASSWORD}
EOF
