#!/bin/sh

if test -t 1; then
  # This is not foolproof. Can easily be beat by doing |cat. This is just to
  # make it less likely that secrets are output to terminal.
  echo "Error: Not outputting secret to stdout; redirect output to a file or" \
    "pipe output to \`sops\`." >/dev/stderr
  exit 1
fi

printf_stderr() {
  printf "$@" > /dev/stderr
}

printf_stderr "Keycloak domain: "
read KEYCLOAK_DOMAIN
printf_stderr "Keycloak realm: "
read KEYCLOAK_REALM

AUTH_PROVIDER_URL="https://${KEYCLOAK_DOMAIN}/realms/${KEYCLOAK_REALM}/.well-known/openid-configuration"
printf_stderr "Attempting to verify OIDC provider.\n"
if ! timeout 1 curl --fail "${AUTH_PROVIDER_URL}" > /dev/null; then
  printf_stderr "\n"
  printf_stderr "Unable to verify OIDC provider using: ${AUTH_PROVIDER_URL}\n"
  exit 1
fi
printf_stderr "OK!\n"

printf_stderr "Client key (as configured in Keycloak): "
read AUTH_PROVIDER_KEY
printf_stderr "Client name (as configured for your application): "
read AUTH_PROVIDER_NAME
printf_stderr "Client secret: "
stty -echo
read AUTH_PROVIDER_SECRET
stty echo
echo > /dev/stderr

cat <<EOF
apiVersion: v1
kind: Secret
metadata:
  name: keycloak-client-config
stringData:
  AUTH_PROVIDER_NAME: ${AUTH_PROVIDER_NAME}
  AUTH_PROVIDER_KEY: ${AUTH_PROVIDER_KEY}
  AUTH_PROVIDER_SECRET: "${AUTH_PROVIDER_SECRET}"
  AUTH_PROVIDER_URL: "${AUTH_PROVIDER_URL}"
EOF
