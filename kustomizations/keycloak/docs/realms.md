By default, Keycloak configures a "master" realm. This realm is used for
administrative purposes, configuring data in Keycloak. In order for Keycloak to
be useful, realms, clients, and users need to be created. Realms are a
collection of clients and users. Clients are third party services that can use
OAuth2 and OpenID Connect to authenticate users.

# Distrust (distrust)

This realm is for members of Distrust, enabling them to log into Distrust
hosted services.

**Clients:**

```
- name: forgejo
  credential_type: client_id_and_secret
```
