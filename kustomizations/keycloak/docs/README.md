# Initial Setup

To generate the admin password for Keycloak, run:

```sh
sh kustomizations/keycloak/scripts/generate-keycloak-secret.sh \
  | sops --encrypt --encrypted-regex '^(data|stringData)$' \
  --input-type=yaml --output-type=yaml /dev/stdin \
  > kustomizations/keycloak/keycloak-config.enc.yaml
```

To get the database credentials, run:

```sh
sops exec-env secrets/production.enc.env 'terraform -chdir=infra/main output -json' | jq '.database_users.value.keycloak' | sops --encrypt --encrypted-regex '^(data|stringData)$' --input-type=json --output-type=yaml /dev/stdin > kustomizations/keycloak/postgres-auth.enc.yaml
```

# Adding Clients

Clients are how Keycloak authenticates a user with a third party service. This
happens by enabling a "Flow" when adding a client. The "Standard" flow has the
user's browser get a short-lived authorization token from Keycloak, send the
authorization token to the client, then the client request a long-lived access
token from Keycloak. This way, the access token is never given to the client.
"Direct Access Grants" means that a user may pass their Keycloak credentials to
the client, then the client may use those credentials to authenticate with
Keycloak and get an access token. In this manner, the client still does not
expose the access token to the user, but the user exposes their Keycloak
credentials to the client.

When a Client is created, the Client Secret can be encrypted to a ksops Secret
using the following script (Forgejo used as an example):

```sh
sh kustomizations/keycloak/scripts/generate-keycloak-client-secret.sh \
  | sops --encrypt --encrypted-regex '^(data|stringData)$' \
  --input-type=yaml --output-type=yaml /dev/stdin \
  > kustomizations/forgejo/keycloak-client-config.enc.yaml
```
