#!/bin/sh

if test -t 1; then
  # This is not foolproof. Can easily be beat by doing |cat. This is just to
  # make it less likely that secrets are output to terminal.
  echo "Error: Not outputting secret to stdout; redirect output to a file or" \
    "pipe output to \`sops\`." >/dev/stderr
  exit 1
fi

tmpdir="$(mktemp -d)"
mkdir -p "$tmpdir/etc/ssh"
ssh-keygen -Af "$tmpdir" 1>&2

cat <<EOF
apiVersion: v1
kind: Secret
metadata:
  name: forgejo-ssh-keys
data:
EOF

for file in $(find "$tmpdir"); do
  if test -f "$file"; then
    echo "  $(basename $file): $(base64 -w 0 $file)"
  fi
done
