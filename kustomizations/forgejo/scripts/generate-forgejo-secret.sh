#!/bin/sh

if test -t 1; then
  # This is not foolproof. Can easily be beat by doing |cat. This is just to
  # make it less likely that secrets are output to terminal.
  echo "Error: Not outputting secret to stdout; redirect output to a file or" \
    "pipe output to \`sops\`." >/dev/stderr
  exit 1
fi

FORGEJO_VERSION="1.20.5-0"
FORGEJO_TAG="sha256:d665129e66cc04fa72aa6a284eb98b17c0eee642aeaad7c88edec21f9edbf519"
FORGEJO_SLUG="${FORGEJO_VERSION}@${FORGEJO_TAG}"

forgejo() {
  # TODO: make this extract image tag from kustomization?
  docker run "codeberg.org/forgejo/forgejo:$FORGEJO_SLUG" forgejo "$@"
}

GITEA__SERVER__LFS_JWT_SECRET="$(forgejo generate secret LFS_JWT_SECRET)"
GITEA__SECURITY__SECRET_KEY="$(forgejo generate secret SECRET_KEY)"
GITEA__SECURITY__INTERNAL_TOKEN="$(forgejo generate secret INTERNAL_TOKEN)"

cat <<EOF
apiVersion: v1
kind: Secret
metadata:
  name: forgejo-config
stringData:
  GITEA__SERVER__LFS_JWT_SECRET: ${GITEA__SERVER__LFS_JWT_SECRET}
  GITEA__SECURITY__SECRET_KEY: ${GITEA__SECURITY__SECRET_KEY}
  GITEA__SECURITY__INTERNAL_TOKEN: ${GITEA__SECURITY__INTERNAL_TOKEN}
EOF
